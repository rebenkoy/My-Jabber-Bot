import subprocess
import time
import datetime
import fcntl
import os
import threading
import socket
import sys
import json
import logging
_d = logging.debug
'''---------------------------commands---------------------------------'''


def set_non_blocking(fd):
    flags = fcntl.fcntl(fd, fcntl.F_GETFL)
    flags = flags | os.O_NONBLOCK
    fcntl.fcntl(fd, fcntl.F_SETFL, flags)


class SubProcessThread(threading.Thread):
    def __init__(self, u_sock, command):
        threading.Thread.__init__(self)
        self.socket = u_sock
        self.command = command
        self.muted = False
        self.error = ''
        self.output = ''
        self.stopped = False
        self.processID = 0
        self.startTime = 0
        self.exit_code = 179

    def send_results(self):
        if self.muted:
            return
        if self.output != '':
            self.socket.send(self.processID, self.startTime, self.output)
            self.output = ""
        if self.error != "":
            self.socket.send(self.processID, self.startTime, self.error, type='error')
            self.error = ""
        if self.stopped:
            self.socket.send(self.processID, self.startTime, self.exit_code, type='exit')

    def run(self):
        process = subprocess.Popen(self.command,
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   bufsize=1)
        self.processID = process.pid
        self.startTime = datetime.datetime.now().time()
        set_non_blocking(process.stdout)
        set_non_blocking(process.stderr)
        flag1 = flag2 = 1
        while flag1 | flag2:
            _d('reading...')
            time.sleep(0.5)
            flag1 = flag2 = 1
            try:
                _d('output:')
                output = process.stdout.read().decode("utf-8")
                if output:
                    self.output += output
                    _d(self.output)
            except OSError:
                flag1 = 0
            try:
                _d('errors:')
                errors = process.stderr.read().decode("utf-8")
                if errors:
                    self.error += errors
                    _d(self.error)
            except OSError:
                flag2 = 0
            flag1 = "" != self.output and flag1
            flag2 = "" != self.error and flag2
            _d('flags: ' + str(flag1) + ' ' + str(flag2))
            self.send_results()
        _d('reading stopped')
        process.wait()
        self.stopped = True
        self.exit_code = process.returncode
        _d('process stopped')
        while not ((self.output == '') & (self.error == '')):
            _d(self.output)
            _d(self.error)
            self.send_results()
            time.sleep(0.5)
        _d('Tread:' + str(self) + ' ended')
        self.socket.Threads.remove(self)
        _d(self.socket.Threads)

    def react(self, command):
        words = command.split(" ")
        pid = words[1]
        st_time = words[2]
        task = words[3]
        arguments = words[4:]
        if pid != str(self.processID) or st_time != str(self.startTime):
            return
        if "mute" == task:
            if '0' == arguments[0]:
                self.muted = False
            elif '1' == arguments[0]:
                self.muted = True


def command_to_thread(command):
    print(command.split(' '))
    return 'Bot_Thread' == (command.split(' '))[0]


def process_msg(u_sock, msg):
    u_sock.running = True
    u_sock.send_dump()
    body = ' '.join(msg)
    _d('msg:')
    _d(body)
    if 'logout' == body:
        u_sock.logout()
        return
    elif 'cd' == body.split(" ")[0]:
        os.chdir(" ".join(body.split(" ")[1:]))
        u_sock.send("None", datetime.datetime.now().time(), "")
        return
    elif command_to_thread(body):
        for thread in u_sock.Threads:
            thread.react(body)
        return
    else:
        _d('starting Thread')
        _d(u_sock.Threads)
        t = SubProcessThread(u_sock, body.split(" "))
        u_sock.Threads.add(t)
        t.start()

'''---------------------------socket-----------------------------------'''


class SocketListner(threading.Thread):
    def __init__(self, addr, client):
        self.user = client
        open('/home/' + self.user + '/Jabber_Bot/log', mode='w').close()
        logging.basicConfig(filename='/home/' + self.user + '/Jabber_Bot/log', level=logging.DEBUG)
        threading.Thread.__init__(self)
        self.socket = socket.socket(socket.AF_UNIX)
        self.socket.connect(addr)
        self.running = True
        self.Threads = set()
        self.dumpname = '/home/' + self.user + '/Jabber_Bot/dump'
        _d('initialized')
        self.dump = open(self.dumpname, mode='w')
        self.dump.close()

    def run(self):
        _d('started')
        while self.running:
            message_size = int(self.socket.recv(8).decode('utf-8'))
            message = self.socket.recv(message_size).decode('utf-8')
            while len(message) < message_size:
                message += self.socket.recv(message_size - len(message)).decode('utf-8')
            message = json.loads(message)[0]
            _d('pre msg:')
            _d(message)
            process_msg(self, message)

    def send(self, pid, start_time, data, m_type='output'):
        message = json.dumps([m_type, str(os.getcwd()), str(pid), str(start_time), str(data)])
        size = '0' * (8 - len(str(len(message)))) + str(len(message))
        message = message
        _d(message)
        if self.running:
            _d('sending :')
            self.socket.send(size.encode())
            self.socket.send(message.encode())
        else:
            _d('writing:')
            self.dump.write(message)
            self.dump.write('\n')
        _d(size)
        _d(message)

    def logout(self):
        self.dump = open(self.dumpname, mode='w')
        _d('dump')
        _d(self.dump)
        self.running = False
        iterations = 0
        while self.Threads != set() and self.running is False:
            _d('set:')
            _d(self.Threads)
            _d('iteration: ')
            _d(iterations)
            iterations += 1
            time.sleep(1)
        _d('closed')
        self.dump.close()
        if self.running:
            self.send_dump()
        else:
            message = json.dumps(['logout', str(os.getcwd()), str(0), str(0), str('Logged out')])
            size = '0' * (8 - len(str(len(message)))) + str(len(message))
            self.socket.send(size.encode())
            self.socket.send(message.encode())

    def send_dump(self):
        self.dump = open(self.dumpname, mode='r')
        data = self.dump.readlines()
        self.dump.close()
        _d(data)
        for message in data:
            message = json.loads(message)
            self.send(message[2], message[3], message[4], message[0])

address = sys.argv[1]
user = sys.argv[2]
sock = SocketListner(address, user)
sock.start()
