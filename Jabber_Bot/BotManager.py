from sleekxmpp import ClientXMPP
import logging
import threading
import socket
import os
import subprocess
import pwd
import select
import json
import fcntl


def set_non_blocking(fd):
    flags = fcntl.fcntl(fd, fcntl.F_GETFL)
    flags = flags | os.O_NONBLOCK
    fcntl.fcntl(fd, fcntl.F_SETFL, flags)


def approved(addr, username, password):
    env = os.environ
    me = pwd.getpwuid(os.getuid()).pw_name
    env['SUDO_ASKPASS'] = '/'.join(addr.split("/")) + '/askpass.py'
    env['USER_PASSWORD'] = password
    sudo_proc = subprocess.Popen(['sudo', '-u', username, '-A', 'whoami'],
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 env=env)
    sudo_err = sudo_proc.stderr.read().decode('utf-8')
    print(sudo_err)
    return me != username and '' == sudo_err


def process_msg(bot, msg):
    sender = msg['from'].bare
    body = (msg['body']).split(' ')
    if sender in bot.clients_online:
        bot.Ulistner.send(sender, body)
        return
    else:
        if 'login' == body[0] and len(body) == 3:
            if approved(bot.addr, body[1], body[2]):
                bot.login(sender, body[1], body[2])
                return
    bot.send_output_message(sender, 'Please log in')


class SocketListner(threading.Thread):
    def __init__(self, bot):
        threading.Thread.__init__(self)
        self.bot = bot

    def run(self):
        while self.bot.present:
            for client in self.bot.sockets.keys():
                sock = self.bot.sockets[client]
                ready = select.select([sock], [], [], 0.001)
                if ready:
                    name = sock.recv(8).decode('utf-8')
                    if name:
                        message_size = int(name)
                        message = sock.recv(message_size).decode('utf-8')
                        while len(message) < message_size:
                            message += sock.recv(message_size - len(message)).decode('utf-8')
                        message = json.loads(message)
                        self.bot.send_output(client, message)

    def send(self, client,  data):
        sock = self.bot.sockets[client]
        data = json.dumps([data])
        size = '0' * (8 - len(str(len(data)))) + str(len(data))
        data = data
        sock.send(size.encode())
        sock.send(data.encode())


class ManagerBot(ClientXMPP):
    def __init__(self, addr, jid, password, host_name, port, ssl):
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(levelname)-8s %(message)s')
        ClientXMPP.__init__(self, jid + '/' + 'Connection Manager', password)
        self.addr = os.getcwd() + '/' + addr
        self.host_name = host_name
        self.port = port
        self.ssl = ssl
        self.present = True
        self.clients_online = set()
        self.add_event_handler("session_start", self.session_start)
        self.add_event_handler("message", self.message)
        self.connect((host_name, port), use_ssl=ssl)
        self.process(block=False)
        self.sockets = {}

        if os.path.exists('/tmp/Manager_socket'):
            os.remove('/tmp/Manager_socket')
        self.socket_server = socket.socket(family=socket.AF_UNIX)
        self.socket_server.bind('/tmp/Manager_socket')
        os.chmod('/tmp/Manager_socket', 0o666)
        self.socket_server.listen(1)
        self.Ulistner = SocketListner(self)
        self.Ulistner.start()
        set_non_blocking(self.socket.fileno())
        print('ended init')

    def session_start(self, event):
        logging.debug(event)
        self.send_presence()
        self.get_roster()

    def message(self, msg):
        if msg['type'] in ('chat', 'normal'):
            process_msg(self, msg)

    def send_output_message(self, client, message):
        self.send_message(mto=client, mbody=message, mtype='chat')

    def send_output(self, client, message):
        m_type = message[0]
        m_dir = message[1]
        pid = message[2]
        st_t = message[3]
        data = message[4]
        if m_type == 'logout':
            self.clients_online.remove(client)
            text = data
        else:
            text = m_dir + '$\nPID: ' + pid + '\t start time: ' + st_t + '\n' + data
        self.send_message(mto=client, mbody=text, mtype='chat')

    def logout(self, client):

        self.clients_online.remove(client)
        self.send_output_message(client, 'Logged out')

    def login(self, client, username, password):
        if client in self.clients_online:
            return
        self.clients_online.add(client)
        environ = os.environ
        environ['SUDO_ASKPASS'] = '/'.join(self.addr.split("/")) + '/askpass.py'
        environ['USER_PASSWORD'] = password
        subprocess.Popen(['sudo', '-u', username, '-A',
                          'python3.4', '/'.join(self.addr.split("/")) + '/PrivateBot.py',
                          '/tmp/Manager_socket', username],
                         env=environ)
        self.sockets[client], data = self.socket_server.accept()
        self.send_output_message(client, 'Logged in')
